package com.dangkhoa.moneymoney;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {
    EditText edtForgotEmail;
    Button btnSubmit;
    ImageButton btnBackToSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        initialize();
    }

    private void initialize() {
        edtForgotEmail = (EditText) findViewById(R.id.edtForgotEmail);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnBackToSignIn = (ImageButton) findViewById(R.id.btnBackToSignIn);

        btnBackToSignIn.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnBackToSignIn: {
                finish();
                break;
            }
            case R.id.btnSubmit: {
                RequestParams params = new RequestParams();
                params.put("email", edtForgotEmail.getText().toString());

                AsyncHttp.POST("/authentication/forgot", params, null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.d("DATA " + response.toString());
                        try {
                            StaticClass.ShowAlertDialog(ForgotActivity.this, "Success", response.getString("successData"), null, null, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        try {
                            StaticClass.ShowAlertDialog(ForgotActivity.this, "Error", errorResponse.getString("errorMessage"), null, null, "OK", null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
