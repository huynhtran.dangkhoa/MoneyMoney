package com.dangkhoa.moneymoney;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import MoneyMoneyClasses.StaticClass;
import MoneyMoneyClasses.AsyncHttp;

import static android.net.Uri.parse;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnSignUp, btnUpdate;
    EditText edtSignUpName,edtSignUpEmail,edtSignUpPassword,edtSignUpConfirm, edtNewPassword;
    CircleImageView edtSignUpAvatar;
    ImageButton btnBackToSignIn;
    LinearLayout new_password_container;

    int REQUEST_CODE_CAMERA = 123;
    int IMAGE_GALLERY_REQUEST = 20;

    String mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initialize ();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initialize(){
        btnSignUp = (Button) findViewById ( R.id.btnSignUp );
        edtSignUpName = (EditText) findViewById ( R.id.su_name );
        edtSignUpEmail = (EditText) findViewById ( R.id.su_email );
        edtSignUpPassword = (EditText) findViewById ( R.id.su_password );
        edtSignUpConfirm = (EditText) findViewById ( R.id.su_confirm );
        edtSignUpAvatar = (CircleImageView) findViewById ( R.id.su_avatar );
        btnBackToSignIn = (ImageButton) findViewById(R.id.btnBackToSignIn);
        new_password_container = (LinearLayout) findViewById(R.id.new_password_container);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        edtNewPassword = (EditText) findViewById(R.id.su_new_password);

        registerForContextMenu(edtSignUpAvatar);

        btnBackToSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        edtSignUpAvatar.setOnClickListener(this);

        mode = StaticClass.getData(getApplicationContext(), "SIGN_UP");
        if (mode.equals("EDIT_ACCOUNT")) {
            edtSignUpName.setText(StaticClass.getData(getApplicationContext(), "name"));
            edtSignUpEmail.setText(StaticClass.getData(getApplicationContext(), "email"));
            if (!StaticClass.getData(getApplicationContext(), "avatar").isEmpty()) {
                edtSignUpAvatar.setBackgroundColor(getResources().getColor(R.color.transparent));
                edtSignUpAvatar.setImageDrawable(StaticClass.ConvertBase64ToDrawable(getApplicationContext(), StaticClass.getData(getApplicationContext(), "avatar")));
            }
            new_password_container.setVisibility(View.VISIBLE);
            btnUpdate.setVisibility(View.VISIBLE);
            btnSignUp.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context_image, menu);

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.camera:
                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), REQUEST_CODE_CAMERA);
                break;
            default:
                Intent intent = new Intent (Intent.ACTION_PICK);

                File pictureDirectory = Environment.getExternalStoragePublicDirectory (Environment.DIRECTORY_PICTURES);
                String pictureDirectoryPath = pictureDirectory.getPath();

                // finally, get a URI representation
                Uri data = Uri.parse(pictureDirectoryPath);

                // set the data and type.  Get all image types.
                intent.setDataAndType(data, "image/*");

                // we will invoke this activity, and get something back from it.
                startActivityForResult(intent, IMAGE_GALLERY_REQUEST);
                break;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if we are here, everything processed successfully.
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_GALLERY_REQUEST) {
                // the address of the image on the SD Card.
                Uri imageUri = data.getData();
                // declare a stream to read the image data from the SD Card.
                InputStream inputStream;

                // we are getting an input stream, based on the URI of the image.
                try {
                    inputStream = getContentResolver().openInputStream(imageUri);
                    // get a bitmap from the stream.
                    Bitmap image = BitmapFactory.decodeStream(inputStream);
                    // show the image to the user
                    Drawable dr = new BitmapDrawable(getResources(), image);
                    edtSignUpAvatar.setImageDrawable(dr);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            if(requestCode == REQUEST_CODE_CAMERA){
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                Drawable dr = new BitmapDrawable(getResources(), photo);
                edtSignUpAvatar.setImageDrawable(dr);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnBackToSignIn: {
                finish();
                break;
            }
            case R.id.btnSignUp: {
                String pw = String.valueOf(edtSignUpPassword.getText());
                String cf = String.valueOf(edtSignUpConfirm.getText());

                if (!pw.equals(cf)) {
                    StaticClass.ShowAlertDialog(getApplicationContext(), "Error", "Passwords do not match.", null, null, "OK", null);
                } else {
                    RequestParams params = new RequestParams();
                    params.put("name", edtSignUpName.getText());
                    params.put ( "email",edtSignUpEmail.getText () );
                    params.put("password", pw);
                    params.put("confirm", cf);
                    params.put("avatar",StaticClass.ConvertDrawableToBase64 ( edtSignUpAvatar.getDrawable () ));
                    AsyncHttp.POST ( "/authentication/register",params,null,new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                            try {
                                StaticClass.ShowAlertDialog(SignUpActivity.this, "Success", response.getString("successData"), null, null, "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                });
                            }catch (JSONException e){
                                e.printStackTrace();

                                //dialogLoading.setVisibility(View.INVISIBLE);
                                try {
                                    String error = response.getString("errorMessage");
                                    StaticClass.ShowAlertDialog(SignUpActivity.this, "Error", error, null, null, "Ok", null);
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    });
                }
                break;
            }
            case R.id.btnUpdate: {
                String pw = String.valueOf(edtNewPassword.getText());
                String cf = String.valueOf(edtSignUpConfirm.getText());

                if (!pw.equals(cf)) {
                    StaticClass.ShowAlertDialog(this, "Error", "Passwords do not match.", null, null, "OK", null);
                } else {
                    RequestParams params = new RequestParams();
                    params.put("id", StaticClass.getData(getApplicationContext(), "id"));
                    params.put("name", edtSignUpName.getText());
                    params.put("old_password", edtSignUpPassword.getText());
                    params.put("new_password", pw);
                    params.put("avatar", StaticClass.ConvertDrawableToBase64 ( edtSignUpAvatar.getDrawable () ));
                    AsyncHttp.PATCH ( "/user/info",params,null,new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                            try {
                                JSONObject object = response.getJSONObject("successData");

                                String token = object.getString("token");
                                if (!token.isEmpty()) {
                                    StaticClass.saveData(getApplicationContext(), "token", token);
                                }

                                StaticClass.ShowAlertDialog(SignUpActivity.this, "Success", object.getString("message"), null, null, "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        StaticClass.saveData(getApplicationContext(), "avatar", StaticClass.ConvertDrawableToBase64(edtSignUpAvatar.getDrawable()));
                                        StaticClass.saveData(getApplicationContext(), "name", String.valueOf(edtSignUpName.getText()));
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        finish();
                                    }
                                });
                            }catch (JSONException e){
                                e.printStackTrace();

                                //dialogLoading.setVisibility(View.INVISIBLE);
                                try {
                                    String error = response.getString("errorMessage");
                                    Logger.d(error);
                                    StaticClass.ShowAlertDialog(SignUpActivity.this, "Error", error, null, null, "Ok", null);
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    });
                }
                break;
            }
            case R.id.su_avatar:
                openContextMenu(view);
                break;
        }
    }
}
