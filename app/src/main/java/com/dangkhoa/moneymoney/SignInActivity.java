package com.dangkhoa.moneymoney;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

public class SignInActivity extends AppCompatActivity {
    Button btnSignIn;
    EditText edtSignInEmail, edtSignInPassword;
    TextView btnGoToRegister;
    ImageButton btnForgotPassword;
    RelativeLayout dialogLoading;

    String id, avatar, name, email, token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        initialize();

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* POST data to API */
                dialogLoading.setVisibility(View.VISIBLE);
                RequestParams params = new RequestParams();
                params.put("email", edtSignInEmail.getText());
                params.put("password", edtSignInPassword.getText());
                AsyncHttp.POST("/authentication/sign_in", params, null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            JSONObject object = response.getJSONObject("successData");
                            id = object.getString("id");
                            StaticClass.saveData(getApplicationContext(), "id", id);

                            avatar = object.getString("avatar");
                            StaticClass.saveData(getApplicationContext(), "avatar", avatar);

                            name = object.getString("name");
                            StaticClass.saveData(getApplicationContext(), "name", name);

                            email = object.getString("email");
                            StaticClass.saveData(getApplicationContext(), "email", email);

                            /* Remember save token!!! */
                            token = object.getString("token");
                            StaticClass.saveData(getApplicationContext(), "token", token);
                            StaticClass.saveData(getApplicationContext(), "type", "expense");

//                            JSONArray cards = object.getJSONArray("cards");
//                            if (cards.length() < 1) {
//                                StaticClass.saveData(getApplicationContext(), "new_user", String.valueOf(true));
//                            }else {
//                                StaticClass.saveData(getApplicationContext(), "new_user", String.valueOf(false));
//                            }

                            dialogLoading.setVisibility(View.INVISIBLE);
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();

                            dialogLoading.setVisibility(View.INVISIBLE);
                            try {
                                String error = response.getString("errorMessage");
                                StaticClass.ShowAlertDialog(SignInActivity.this, "Error", error, null, null, "Ok", null);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        dialogLoading.setVisibility(View.INVISIBLE);
                        Logger.e("ERROR: " + errorResponse);
                        try {
                            String error = errorResponse.getString("errorMessage");
                            StaticClass.ShowAlertDialog(SignInActivity.this, "Error", error, null, null, "Ok", null);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                });
            }
        });

        btnGoToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticClass.saveData(getApplicationContext(), "SIGN_UP", "NEW_ACCOUNT");
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });

        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* Do something... */
                Intent intent = new Intent(getApplicationContext(), ForgotActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initialize() {
        String token = StaticClass.getData(getApplicationContext(), "token");

        if (token != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }

        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        edtSignInEmail = (EditText) findViewById(R.id.edtSignInEmail);
        edtSignInPassword = (EditText) findViewById(R.id.edtSignInPassword);
        btnGoToRegister = (TextView) findViewById(R.id.btnGoToRegister);
        btnForgotPassword = (ImageButton) findViewById(R.id.btnForgotPassword);
        dialogLoading = (RelativeLayout) findViewById(R.id.dialogLoading);
    }
}
