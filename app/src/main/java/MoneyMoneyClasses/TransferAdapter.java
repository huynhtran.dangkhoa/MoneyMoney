package MoneyMoneyClasses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dangkhoa.moneymoney.R;

import java.util.ArrayList;

/**
 * Created by dangkhoa on 10/20/17.
 */

public class TransferAdapter extends RecyclerView.Adapter<TransferAdapter.ViewHolder> {
    ArrayList<TransferItem> transferItems;
    Context context;

    public TransferAdapter(ArrayList<TransferItem> transferItems, Context context) {
        this.transferItems = transferItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.transfer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtTransferFrom.setText(transferItems.get(position).getFrom());
        holder.txtTransferTo.setText(transferItems.get(position).getTo());
        holder.txtTransferDate.setText(transferItems.get(position).getDatetime());
        holder.txtTransferValue.setText(transferItems.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return transferItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTransferFrom, txtTransferTo, txtTransferDate, txtTransferValue;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTransferFrom = (TextView) itemView.findViewById(R.id.txtTransferFrom);
            txtTransferTo = (TextView) itemView.findViewById(R.id.txtTransferTo);
            txtTransferDate = (TextView) itemView.findViewById(R.id.txtTransferDate);
            txtTransferValue = (TextView) itemView.findViewById(R.id.txtTransferValue);
        }
    }
}
