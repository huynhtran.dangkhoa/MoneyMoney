package MoneyMoneyClasses;

import android.graphics.drawable.Drawable;

import java.util.Comparator;

/**
 * Created by dangkhoa on 8/13/17.
 */

public class CategoryItem {
    Drawable Icon;
    private String Category, Price;

    public CategoryItem(Drawable icon, String category, String price) {
        Icon = icon;
        Category = category;
        Price = price;
    }

    public Drawable getIcon() {
        return Icon;
    }

    public void setIcon(Drawable icon) {
        Icon = icon;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }
}
