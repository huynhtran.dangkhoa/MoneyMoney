package MoneyMoneyClasses;

/**
 * Created by dangkhoa on 10/20/17.
 */

public class TransferItem {
    String From, To, Datetime, Value;

    public TransferItem(String from, String to, String datetime, String value) {
        From = from;
        To = to;
        Datetime = datetime;
        Value = value;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        To = to;
    }

    public String getDatetime() {
        return Datetime;
    }

    public void setDatetime(String datetime) {
        Datetime = datetime;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
