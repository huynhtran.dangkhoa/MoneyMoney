package MoneyMoneyClasses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by DELL on 8/19/2017.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {
    ArrayList<NoteItem> noteItems;
    Context context;
    public NoteAdapter(ArrayList<NoteItem> noteItems,Context context){
        this.noteItems = noteItems;
        this.context = context;
    }
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.note, parent, false);
        return new ViewHolder(view);
    }
    public void onBindViewHolder(NoteAdapter.ViewHolder holder, int position){
        holder.txtTitle.setText(noteItems.get(position).getTitle());
        holder.txtDate.setText(noteItems.get(position).getDate());
    }
    public int getItemCount(){return noteItems.size();}

    public void removeItem(int position) {
        RequestParams params = new RequestParams();
        params.put("id", noteItems.get(position).getId());
        AsyncHttp.DELETE("/note/delete", params, StaticClass.getToken(context), new JsonHttpResponseHandler () {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.d("DELETE: " + response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
        noteItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, noteItems.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle, txtDate;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.item_title);
            txtDate = (TextView) itemView.findViewById(R.id.item_date);
        }

    }

}
