package MoneyMoneyClasses;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dangkhoa.moneymoney.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * Created by dangkhoa on 9/16/17.
 */

public class ContextMenuAdapter extends BaseAdapter {
    Context context;
    int layout;
    List<ContextMenu> contextMenus;

    public ContextMenuAdapter(Context context, int layout, List<ContextMenu> contextMenus) {
        this.context = context;
        this.layout = layout;
        this.contextMenus = contextMenus;
    }

    @Override
    public int getCount() {
        return contextMenus.size();
    }

    @Override
    public Object getItem(int i) {
        return contextMenus.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(context);

        if (view == null) {
            view = inflater.inflate(layout, null);
            holder = new ViewHolder();
            holder.contextMenuKey = (TextView) view.findViewById(R.id.contextMenuKey);
            holder.contextMenuValue = (TextView) view.findViewById(R.id.contextMenuValue);

//            new ContextMenuTask(holder, i, contextMenus).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }

        holder.contextMenuKey.setText(contextMenus.get(i).getKey());
        holder.contextMenuValue.setText(contextMenus.get(i).getValue());

        return view;
    }

    private static class ViewHolder {
        public TextView contextMenuKey, contextMenuValue;
    }
}
