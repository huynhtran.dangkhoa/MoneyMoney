package Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.dangkhoa.moneymoney.MainActivity;
import com.dangkhoa.moneymoney.R;
import com.dangkhoa.moneymoney.SignInActivity;
import com.dangkhoa.moneymoney.SignUpActivity;

import MoneyMoneyClasses.StaticClass;

/**
 * Created by dangkhoa on 8/13/17.
 */

public class SettingsFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    RelativeLayout moneyNav, btnSettingsAccount, btnLanguage, btnSettingsSignOut;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         final String TAG = "MyActivity";
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        initialize(view);

        return view;
    }

    private void initialize(View view) {
        moneyNav = (RelativeLayout) view.findViewById(R.id.moneyNav);
        moneyNav.addView(StaticClass.initNavButton(view.getContext(), view));

        btnSettingsAccount = (RelativeLayout) view.findViewById(R.id.btnSettingsAccount);
        btnLanguage = (RelativeLayout) view.findViewById(R.id.btnSettingsLanguage);
        btnSettingsSignOut = (RelativeLayout) view.findViewById(R.id.btnSettingsSignOut);

        btnSettingsAccount.setOnClickListener(this);
        btnLanguage.setOnClickListener(this);
        btnSettingsSignOut.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnSettingsLanguage: {
                startActivityForResult(new Intent(Settings.ACTION_LOCALE_SETTINGS), 0);
                break;
            }
            case R.id.btnSettingsSignOut: {
                StaticClass.removeAllData(getContext());
                Intent intent = new Intent(getContext(), SignInActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            }
            case R.id.btnSettingsAccount: {
                StaticClass.saveData(getContext(), "SIGN_UP", "EDIT_ACCOUNT");
                StaticClass.saveData(getContext(), "STATE", "SETTINGS_FRAGMENT");
                startActivity(new Intent(getContext(), SignUpActivity.class));
                getActivity().finish();
                break;
            }
        }
    }
}
