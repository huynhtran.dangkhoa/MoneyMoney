package Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.RecordAdapter;
import MoneyMoneyClasses.RecordItem;
import MoneyMoneyClasses.StaticClass;
import MoneyMoneyClasses.SwipeToDeleteCallback;
import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 9/2/17.
 */

public class RecordFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    RecyclerView recyclerView;
    ImageButton btnBackToHome, btnAddNew;
    TextView totalRecord;
    PullRefreshLayout pullRefreshLayout;
    RelativeLayout dialogLoading;

    Bundle bundle;
    String route;
    RequestParams params = new RequestParams();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_record, container, false);

        initialize(view);

        return view;
    }

    private void initialize(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_record);
        btnBackToHome = (ImageButton) view.findViewById(R.id.btnBackToHome);
        btnAddNew = (ImageButton) view.findViewById(R.id.btnAddNew);
        totalRecord = (TextView) view.findViewById(R.id.totalRecord);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefreshLayout);
        dialogLoading = (RelativeLayout) view.findViewById(R.id.dialogLoading);

        btnBackToHome.setOnClickListener(this);
        btnAddNew.setOnClickListener(this);

        bundle = getArguments();
        Logger.d("CATEGORY: " + bundle);
        if (bundle != null) {
            route = "/records/" + StaticClass.getData(getContext(), "type") + "/" + bundle.getString("category");
            requestToServer(route);

            pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    pullRefreshLayout.setRefreshing(true);
                    requestToServer(route);
                }
            });
        }
    }

    private void setUpRecyclerViewList(@NonNull RecyclerView recyclerView, @NonNull JSONArray array) {
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.custom_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        final ArrayList<RecordItem> recordItems = new ArrayList<>();
        int total = 0;
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);

                total = total + object.getInt("value");

                String datetime = object.getString("time");

                recordItems.add(new RecordItem(
                        object.getString("_id"),
                        datetime.substring(0, datetime.indexOf(' ')),
                        datetime.substring(datetime.indexOf(' ') + 1, datetime.length()),
                        object.getString("mode"),
                        object.getString("card"),
                        object.getString("category"),
                        StaticClass.ConvertStringToDecimal(object.getString("value")),
                        object.getString("note"),
                        object.getString("picture"),
                        StaticClass.getIconDetail(getContext(), object.getString("category"))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        totalRecord.setText(String.valueOf(StaticClass.ConvertStringToDecimal(String.valueOf(total))));

        final RecordAdapter recordAdapter = new RecordAdapter(recordItems, getContext());
        recyclerView.setAdapter(recordAdapter);

        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(0, ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT, getContext()) {
            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    RequestParams params = new RequestParams();
                    params.put("id", recordItems.get(viewHolder.getAdapterPosition()).getId());
                    AsyncHttp.DELETE("/record/delete", params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            recordAdapter.removeItem(viewHolder.getAdapterPosition());
                            requestToServer(route);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                }else {
                    int index = viewHolder.getAdapterPosition();

                    Bundle bundle = new Bundle();
                    bundle.putString("id", recordItems.get(index).getId());
                    bundle.putString("date", recordItems.get(index).getDate());
                    bundle.putString("time", recordItems.get(index).getTime());
                    bundle.putString("type", recordItems.get(index).getType());
                    bundle.putString("card", recordItems.get(index).getCard());
                    bundle.putString("category", recordItems.get(index).getCategory());
                    bundle.putString("value", recordItems.get(index).getValue());
                    bundle.putString("note", recordItems.get(index).getNote());
                    bundle.putString("picture", recordItems.get(index).getPicture());
                    bundle.putBoolean("isEditing", true);
                    bundle.putString("state", "RECORD_EDIT");

                    StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), false, new AddNewExpenseOrIncome(), bundle);
                }
            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(swipeToDeleteCallback);
        helper.attachToRecyclerView(recyclerView);
    }

    private void requestToServer(@NonNull String route) {
        params.put("id", StaticClass.getData(getContext(), "id"));

        AsyncHttp.GET(route, params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                dialogLoading.setVisibility(View.INVISIBLE);
                pullRefreshLayout.setRefreshing(false);
                Logger.d("RECORD: " + response);
                try {
                    JSONArray arrRecords = response.getJSONArray("successData");
                    setUpRecyclerViewList(recyclerView, arrRecords);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                pullRefreshLayout.setRefreshing(false);
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnBackToHome: {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new ExpenseIncomeFragment(), null);
                break;
            }
            case R.id.btnAddNew: {
                Bundle bundle = new Bundle();
                bundle.putString("state", "RECORD_NEW");
                bundle.putString("category", this.bundle.getString("category"));
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), false, new AddNewExpenseOrIncome(), bundle);
                break;
            }
        }
    }
}
