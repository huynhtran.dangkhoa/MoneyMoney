package Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.CardAdapter;
import MoneyMoneyClasses.CardItem;
import MoneyMoneyClasses.ContextMenu;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 7/30/17.
 */

public class CardManageFragment extends android.support.v4.app.Fragment {
    RelativeLayout moneyNav;
    RecyclerView recyclerView;
    ImageButton btnNew;

    public static ArrayList<ContextMenu> cards = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_manage, container, false);

        initialize(view);
        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), false, new AddNewCard(), null);
            }
        });

        return  view;
    }

    private void initialize(View view) {
        moneyNav = (RelativeLayout) view.findViewById(R.id.moneyNav);
        moneyNav.addView(StaticClass.initNavButton(view.getContext(), view));
        btnNew = (ImageButton) view.findViewById ( R.id.btnNew );
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_card);

        cards.clear();
        String id = StaticClass.getData(getContext(), "id");
        RequestParams params = new RequestParams();
        params.put("id", id);
        AsyncHttp.GET("/cards", params, StaticClass.getToken(getContext()), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray arrCards = response.getJSONArray("successData");
                    setUpRecyclerViewList(arrCards);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    private void setUpRecyclerViewList(JSONArray jsonArray) {
        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        DecimalFormatSymbols symbol = new DecimalFormatSymbols();
        symbol.setDecimalSeparator(',');
        symbol.setGroupingSeparator(',');
        DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();
        format.setDecimalFormatSymbols(symbol);

        ArrayList<CardItem> cardItems = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {

            try {
                Logger.d("DATA: " + jsonArray.get(i));
                JSONObject object = jsonArray.getJSONObject(i);
                Logger.d("IMAGE: " + object.getString("image"));
                cardItems.add(new CardItem(
                        StaticClass.ConvertBase64ToDrawable(getContext(),
                        object.getString("image")),
                        object.getString ( "name" ),
                        StaticClass.ConvertStringToDecimal(object.getInt("balance"))
                        ));
                cards.add(new MoneyMoneyClasses.ContextMenu(object.getString("name"), object.getString("name") + " - " + object.getString("number"), object.getString("_id")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        CardAdapter categoryAdapter = new CardAdapter(cardItems, getContext());
        recyclerView.setAdapter(categoryAdapter);
    }
}