package Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.dangkhoa.moneymoney.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;

import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;

import MoneyMoneyClasses.AsyncHttp;
import MoneyMoneyClasses.StaticClass;
import cz.msebera.android.httpclient.Header;

/**
 * Created by dangkhoa on 8/23/17.
 */

public class AddNoteFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    ImageButton btnBackToNote,btnTick;
    EditText edtTittle,edtContent;
    Calendar calendar = Calendar.getInstance();
    RequestParams params = new RequestParams();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_note, container, false);

        initialize(view);

        btnBackToNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new NoteFragment(), null);
            }
        });

        return view;
    }

    private void initialize(View view) {
        btnBackToNote = (ImageButton) view.findViewById(R.id.btnBackToNote);
        btnTick = (ImageButton) view.findViewById ( R.id.btnAddNewNote );
        edtTittle = (EditText) view.findViewById ( R.id.edt_add_tittle );
        edtContent = (EditText) view.findViewById ( R.id.edt_add_content );
        btnTick.setOnClickListener ( this );

    }
    public void onClick(View view){
        int Vid =view.getId ();
        switch (Vid){
            case R.id.btnAddNewNote:{
                String token = StaticClass.getData(getContext(), "token");
                String id = StaticClass.getData(getContext(), "id");

                params.put("id", id);
                params.put("title",edtTittle.getText ());
                params.put("content", edtContent.getText ());

                AsyncHttp.POST("/note/create", params, token, new JsonHttpResponseHandler () {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.d("RESULT: " + response);
                        StaticClass.GoToNewFragmentWithAnimation(getFragmentManager(), true, new NoteFragment (), null);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Logger.e("ERROR: " + errorResponse);
                    }
                });
                break;}
        }
    }
}
